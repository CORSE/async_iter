# Async iter

Prototype for idea of using long loops in asynchronous manner that will choose
automatically with some measurement of time when to give the
executor control again.


## Bench à faire

- micro bench de méthode classique vs fold [x] (pas de différence massive)
- problemes de timer si petite task -> boucle avec exponential [x]
- micro bench iter de base vs iter async et rien qui tourne [x]

`hyperfine --warmup 2 "target/debug/examples/const_step v0 $ITER $TASK" "target/debug/examples/const_step v1 $ITER $TASK" "target/debug/examples/const_step v2 $ITER $TASK"`

## Résultats du bench

- 1 % de différence entre v0 (iter sans async) et iter (v1 et v2) avec async.
- souvent pas dans le sens intuitif (async + rapide alors qu'il y a un yield).
- la différence tend vers 0 si on met des taches très longues.
- pour les courtes taches (<1ms) il y a de grandes variances dans la méthode 
de mesure de temps avec 1 itération uniquement -> donc on fait la loop avec exponential grow.

## Autres idées
- trucs plus complexes avec Producer et/ou Consumer []
possiblement non nécessaire vu que les 2 méthodes semblent équivalentes et qu'il n'y a pas de surcout.


