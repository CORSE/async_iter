extern crate async_iter;

use std::time::Duration;

use futures::executor::block_on;
use lazy_static::lazy_static;

lazy_static! {
    static ref ARGS: Vec<String> = {
        let args: Vec<String> = std::env::args().collect();

        if args.len() != 4 {
            println!("args = {:?}", args);
            panic!("Invalid number of arguments expected 3: version n_iters task_micro_sec");
        }
        args
    };
    static ref IMPLEM_VERSION: String = ARGS[1].clone();
    static ref N_ITERS: u16 = ARGS[2].parse().unwrap();
    static ref MICRO_SEC_WAIT: u64 = ARGS[3].parse().unwrap();
}

fn main() {
    match IMPLEM_VERSION.as_str() {
        "v1" => {
            use async_iter::SimpleAsyncIter;
            block_on(async {
                (1..=*N_ITERS).for_each_async_auto(task).await;
            });
        }
        "v2" => {
            use async_iter::SimpleAsyncIterV2;
            block_on(async {
                (1..=*N_ITERS).for_each_async_auto(task).await;
            });
        }

        _ => {
            (1..=*N_ITERS).for_each(task);
        }
    }
}

fn task(_i: u16) {
    std::thread::sleep(Duration::from_micros(*MICRO_SEC_WAIT));
}
