use core::future::Future;
use core::task::Poll;

struct YieldNow(bool);

impl Future for YieldNow {
    type Output = ();
    fn poll(
        mut self: core::pin::Pin<&mut Self>,
        cx: &mut core::task::Context<'_>,
    ) -> Poll<Self::Output> {
        if self.0 {
            Poll::Ready(())
        } else {
            self.0 = true;
            cx.waker().wake_by_ref();
            Poll::Pending
        }
    }
}
pub(crate) fn yield_now() -> impl Future<Output = ()> {
    YieldNow(false)
}
