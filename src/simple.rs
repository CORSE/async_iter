use crate::helpers::yield_now;

pub trait SimpleAsyncIter: Iterator + Sized {
    async fn for_each_async_steps<F: Fn(Self::Item)>(mut self, f: F, n: usize) {
        if n == 0 {
            self.for_each(f);
            return;
        }
        loop {
            for _ in 0..n {
                if let Some(i) = self.next() {
                    f(i);
                } else {
                    return;
                }
            }
            yield_now().await;
        }
    }

    #[cfg(feature = "std")]
    async fn for_each_async_auto<F: Fn(Self::Item)>(mut self, f: F) {
        const LATENCY_MAX_SEC: f32 = 1.0 / 60.0; // 1 frame per second on a usual 60 Hz monitor
        let one_iter_time = {
            let start = std::time::SystemTime::now();
            let mut time = 0.0;
            let mut n_iters = 1;
            while time < LATENCY_MAX_SEC {
                for _ in 0..n_iters {
                    if let Some(i) = self.next() {
                        f(i);
                    } else {
                        return;
                    }
                }
                time = start.elapsed().unwrap().as_secs_f32();
                n_iters *= 2;
            }
            n_iters /= 2;
            time / (n_iters as f32)
        };
        let step = (LATENCY_MAX_SEC / one_iter_time).ceil() as usize;
        println!("step v1 = {}", step);
        self.for_each_async_steps(f, step).await;
    }
}

impl<T: ExactSizeIterator> SimpleAsyncIter for T {}
